from threading import Thread
from logging import getLogger

logger = getLogger()


class LoRaGatewayConnection(Thread):
    def __init__(self, connection):
        super(LoRaGatewayConnection, self).__init__()
        self.__socket_connection = connection
        self.__destroyed = False

    def run(self):
        while not self.__destroyed:
            data = self.__socket_connection.recv(4096)
            reply = "OK... " + data

            if not data:
                break

            self.__socket_connection.sendall(reply)
            logger.info("sent {} to {} as reply to {}".format(reply, self.__socket_connection.getsockname(), data))

    def destroy(self):
        logger.info('Destroying {}'.format(self))
        self.__destroyed = True
        self.__socket_connection.close()
        logger.info('{} destroyed'.format(self))
