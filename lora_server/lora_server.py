from threading import Thread
from logging import getLogger
from socket import socket, AF_INET, SOCK_STREAM, timeout
from lora_gateway_connection import LoRaGatewayConnection


logger = getLogger()


class LoRaServer(Thread):
    MAX_NB_OF_GATEWAY_CONNECTIONS = 10

    def __init__(self, host, port):
        super(LoRaServer, self).__init__()
        self.__socket = socket(AF_INET, SOCK_STREAM)
        self.__socket.bind((host, port))
        self.__socket.listen(self.MAX_NB_OF_GATEWAY_CONNECTIONS)
        self.__socket.settimeout(.01)
        self.__destroyed = False
        self.__lora_gateway_connections = []

    def run(self):
        while not self.__destroyed:
            try:
                connection, address = self.__socket.accept()
                logger.info('Connected with {}:{}'.format(address[0], address[1]))
                lora_gateway_connection = LoRaGatewayConnection(connection)
                self.__lora_gateway_connections.append(lora_gateway_connection)
                lora_gateway_connection.start()
            except timeout:
                pass

    def destroy(self):
        logger.info('Destroying {}'.format(self))
        self.__destroyed = True
        for connection in self.__lora_gateway_connections:
            connection.destroy()
            connection.join(2)
        self.__socket.close()
        logger.info('{} destroyed'.format(self))

