from logging import StreamHandler, Formatter, INFO, getLogger
from lora_server.lora_server import LoRaServer

logger = getLogger()


def main():
    setup_logger()
    lora_server = LoRaServer("localhost", 8888)
    lora_server.start()

    try:
        while True:
            pass
    except KeyboardInterrupt:
        lora_server.destroy()
        lora_server.join(2)
    logger.info("end script")


def setup_logger(level=INFO):
    logger.setLevel(level)

    handler = StreamHandler()

    formatter = Formatter('%(asctime)s :: [%(levelname)s] :: %(filename)s :: %(message)s')
    formatter.datefmt = '%d/%m/%Y %H:%M:%S'

    handler.setFormatter(formatter)
    logger.addHandler(handler)


if __name__ == "__main__":
    main()
